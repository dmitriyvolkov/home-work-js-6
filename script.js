function createNewUser(name, lastname) {
  const firstName = prompt('What your name ?')
  const lastName = prompt('What your last name')
  const userOld = prompt('How old are  you?')
  const data = new Date()
  const newUser = {
    name: firstName,
    lastname: lastName,
    birthday: userOld,
    getLogin: function () {
      return `${this.name.substring(0, 1)}${this.lastname}`.toLowerCase()
    },
    getAge: function () {
      return data.getFullYear() - this.birthday.slice(6)
    },
    getPassword: function () {
      return `${this.name
        .substring(0, 1)
        .toUpperCase()}${this.lastname.toLowerCase()}${this.birthday.slice(6)}`
    },
  }

  return newUser
}
const newUser = createNewUser()
console.log(newUser)
console.log(newUser.getLogin())
console.log(newUser.getAge())
console.log(newUser.getPassword())
